FROM alpine
RUN mkdir /build
WORKDIR /build
RUN wget https://github.com/schollz/croc/releases/download/v8.4.0/croc_8.4.0_Linux-64bit.tar.gz -O croc.tar.gz && tar xzvf croc.tar.gz
RUN mv croc /usr/bin
WORKDIR /root
RUN rm -Rf /build
ENTRYPOINT [ "/usr/bin/croc" ]